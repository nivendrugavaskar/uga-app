//
//  NSString+Validation.m
//  Belinda
//
//  Created by Nivendru on 27/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "NSString+Validation.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Validation)

+(BOOL)validation:(NSString *)sendstring
{
    NSString *trimmed=sendstring;
    trimmed=[trimmed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([trimmed length]==0)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

#pragma MD5 CONVERTION
- (NSString *)MD5String
{
    const char *cstr = [self UTF8String];
    unsigned char result[16];
    CC_MD5(cstr, (CC_LONG)strlen(cstr), result);
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];  
}

// for check

+ (NSString*)md5HexDigest:(NSString*)input {
    const char* str = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}
@end
