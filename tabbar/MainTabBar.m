//
//  MainTabBar.m
//  tabbar
//
//  Created by Nivendru Gavaskar on 16/01/15.
//  Copyright (c) 2015 Nivendru Gavaskar. All rights reserved.
//

#import "MainTabBar.h"

@interface MainTabBar ()

@end

@implementation MainTabBar

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tabBar.hidden=YES;
//    UINavigationController *nav=self.moreNavigationController;
//    nav.navigationBar.hidden=YES;
    
    
    NSLog(@"tabbarheight=%f",self.tabBar.frame.size.height);
    
    _tabbarscroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-self.tabBar.frame.size.height, self.tabBar.frame.size.width, self.tabBar.frame.size.height)];
    _tabbarscroller.backgroundColor=[UIColor lightGrayColor];
    [_tabbarscroller setContentSize:CGSizeMake(self.tabBar.frame.size.width, 0)];
    _tabbarscroller.layer.borderColor=[UIColor redColor].CGColor;
    _tabbarscroller.layer.borderWidth=1.0;
    _tabbarscroller.showsHorizontalScrollIndicator=NO;
    
    [_tabbarscroller setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab.png"]]];
    
    [self addbuttons];
    [self.view addSubview:_tabbarscroller];
    
}
-(void)addbuttons
{
    for (int i=0; i<=3; i++)
    {
        UIButton *btn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/4,self.tabBar.frame.size.height)];
        btn.tag=i;
        btn.frame=CGRectMake(self.view.frame.size.width/4*i, 0, self.view.frame.size.width/4, self.tabBar.frame.size.height);
        btn.layer.borderWidth=1.0;
        btn.layer.borderColor=[UIColor redColor].CGColor;
        [btn addTarget:self action:@selector(buttontapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIImageView *Img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/4, self.tabBar.frame.size.height)];
        Img.tag=i;
        Img.frame=CGRectMake((self.view.frame.size.width/4*i)+(((self.view.frame.size.width/4)-32)/2), 9, 32, 32);
        Img.contentMode=UIViewContentModeScaleAspectFill;
        Img.clipsToBounds=YES;
        
        switch (btn.tag)
        {
            case 0:
                Img.image=[UIImage imageNamed:@"calcselected.png"];
                break;
            case 1:
                Img.image=[UIImage imageNamed:@"settingsselec.png"];
                break;
            case 2:
                Img.image=[UIImage imageNamed:@"helpselec.png"];
                break;
            case 3:
                Img.image=[UIImage imageNamed:@"infoselec.png"];
                break;
                
            default:
                break;
        }
        [_tabbarscroller addSubview:btn];
        [_tabbarscroller addSubview:Img];
    }
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setactivemode:0];
}
-(void)setactivemode:(int)tag
{
//    for (UIButton *local in _tabbarscroller.subviews)
//    {
//        if([local isKindOfClass:[UIButton class]])
//        {
//            if(local.tag==tag)
//            {
//                local.backgroundColor=[UIColor yellowColor];
//                
//            }
//            else
//            {
//                local.backgroundColor=[UIColor clearColor];
//            }
//        }
//    }
    
    // new code
    for (UIImageView *img in _tabbarscroller.subviews)
    {
        if([img isKindOfClass:[UIImageView class]])
        {
            switch (tag)
            {
                case 0:
                    img.image=[UIImage imageNamed:@"calcselected.png"];
                    break;
                case 1:
                    img.image=[UIImage imageNamed:@"settingsselec.png"];
                    break;
                case 2:
                    img.image=[UIImage imageNamed:@"helpselec.png"];
                    break;
                case 3:
                    img.image=[UIImage imageNamed:@"infoselec.png"];
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    [self deselectOtherButton:tag];
}

-(void)deselectOtherButton:(int )tager
{
    
    for (UIView *v in _tabbarscroller.subviews) {
        
        if([v isKindOfClass:[UIImageView class]])
        {
            
            UIImageView *img = (UIImageView *)v;
            if(v.tag==tager) continue;
            
            switch (v.tag)
            {
                case 0:
                    img.image=[UIImage imageNamed:@"calc.png"];
                    break;
                case 1:
                    img.image=[UIImage imageNamed:@"settings.png"];
                    break;
                case 2:
                    img.image=[UIImage imageNamed:@"help.png"];
                    break;
                case 3:
                    img.image=[UIImage imageNamed:@"Information.png"];
                    break;
                    
                default:
                    break;
            }
        }
    }
}


-(void)buttontapped:(id)sender
{
    
    UIButton *btn=(UIButton *)sender;
    [self deselectOtherButton:btn.tag];
    
    
    // new code
    for (UIView *v in _tabbarscroller.subviews)
    {
        if([v isKindOfClass:[UIImageView class]])
        {
            UIImageView *img = (UIImageView *)v;
            if (btn.tag != img.tag) {
                continue;
            }
            switch (btn.tag)
            {
                case 0:
                    img.image=[UIImage imageNamed:@"calcselected.png"];
                    break;
                case 1:
                    img.image=[UIImage imageNamed:@"settingsselec.png"];
                    break;
                case 2:
                    img.image=[UIImage imageNamed:@"helpselec.png"];
                    break;
                case 3:
                    img.image=[UIImage imageNamed:@"infoselec.png"];
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    [self setSelectedViewController:[self.viewControllers objectAtIndex:btn.tag]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
