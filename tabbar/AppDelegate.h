//
//  AppDelegate.h
//  tabbar
//
//  Created by Nivendru Gavaskar on 15/01/15.
//  Copyright (c) 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

